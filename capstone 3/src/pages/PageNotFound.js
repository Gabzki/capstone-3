import Banner from '../components/Banner';

import { Link } from 'react-router-dom';




export default function PageNotFound() {


	return (
		<>
		<h1>Page not found</h1>
		<h4>Go back to <a style={{ textDecoration: 'none'}} href="/">homepage</a></h4> 
		</ >

	)
};
