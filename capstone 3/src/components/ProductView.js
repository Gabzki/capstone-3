import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import { useParams, useNavigate, Link  } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';


export default function ProductView() {

	const {user} = useContext(UserContext);

	const navigate = useNavigate();

	const {productId} = useParams();
	
	const [productName, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState("");
	const [quantity, setQuantity] = useState(1);

	const addToCart = (productId) => {
		fetch(`${process.env.REACT_APP_API_URL}/orders/new-order`, {
			method: "POST",
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({

				productId: productId,
				quantity: quantity,


			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if(data !== true){
				Swal.fire({
				  title: "Successfully added",
				  icon: "success",
				  text: "You have successfully added the item to your cart."
				})

				navigate("/products");

				} else {
				  Swal.fire({
				  title: "Something went wrong",
				  icon: "error",
				  text: "Please try again."
				})
			}
		})
	};


	useEffect(() => {

		console.log(productId);

		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}?quantity=${quantity}`)
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setName(data.productName);
			setDescription(data.description);
			setPrice(data.price);



		})

	}, [productId])
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\

	
	return (

		<Container>
			<Row>
				<Col lg={{span: 6, offset:3}} >
					<Card>
					      <Card.Body className="text-center">
					        <Card.Title>{productName}</Card.Title>
					        <Card.Subtitle>Description:</Card.Subtitle>
					        <Card.Text>{description}</Card.Text>
					        <Card.Subtitle>Price:</Card.Subtitle>
					        <Card.Text>PhP {price}</Card.Text>
					       
					        
					        {
					        	(user.id !== null) ?
					        		<Button variant="primary" onClick={() => addToCart(productId)} >Add to Cart</Button>
					        		:
					        		<Button className="btn btn-danger" as={Link} to="/login"  >Log in to Add to your Cart</Button>
					        }

					      </Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>

	)
}

