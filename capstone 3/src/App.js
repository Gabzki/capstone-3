  import { useState, useEffect } from "react";
import { UserProvider } from './UserContext';
import {Container} from 'react-bootstrap';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom'
import './App.css';
import AppNavbar from './components/AppNavbar';
import ProductView from './components/ProductView';
import Home from './pages/Home';
import Products from './pages/Products';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import PageNotFound from './pages/PageNotFound';
import BootstrapCarousel from './components/BootstrapCarousel';

//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

function App() {

const[user, setUser] = useState({
   id: null,
   isAdmin: null
});

  const unsetUser = () => {
    localStorage.clear();
  }

useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
        headers: {
            Authorization: `Bearer ${localStorage.getItem('token')}`
        }
    })
    .then(res => res.json())
    .then(data => {

        if(typeof data._id !== "undefined") {

            setUser({
                id: data._id,
                isAdmin: data.isAdmin
            })
        } 

        else { 
            setUser({
                id: null,
                isAdmin: null
            })
        }
    })
}, []);

//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


 return (

   <> 
   <UserProvider value={{user, setUser, unsetUser}}>

   <Router>
     <AppNavbar />
   
     <Container>
       <Routes>
         <Route path="/" element={<Home />} />
         <Route path="/Products" element={<Products />} />
        <Route path="/products/:productId" element={<ProductView />} />
         <Route path="/register" element={<Register />} />
         <Route path="/login" element={<Login />} />
         <Route path="/logout" element={<Logout />} />
          <Route path='/*' element={<PageNotFound />}/>
       </Routes>
     </Container>
   </Router>
   </UserProvider>


   </>
  );
}

export default App;


 